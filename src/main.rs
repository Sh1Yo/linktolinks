extern crate regex;
use regex::Regex;
use std::io::BufRead;
use std::io;

fn main() {
    let re_host = Regex::new(r"([\w\.-]+\..+?/)").unwrap();
    let re_without_http = Regex::new(r"(\w+?[\w\./-]+$)").unwrap();
    let re_path = Regex::new(r"(/[\w\./-]+)").unwrap();
    let re_delete_path = Regex::new(r"/[\w\.-]*$").unwrap();
    let re_delete_ports = Regex::new(r"((:(443|80))?/?$)").unwrap();

    for link in io::stdin().lock().lines() {
        let link = match link {
            Ok(val) => val,
            Err(_) => break,
        };

        let caps_host = match re_host.captures(&link) {
            Some(val) => val,
            None => continue,
        };

        let caps_without_http = match re_without_http.captures(&link) {
            Some(val)  => val,
            None => continue,
        };

        let caps_path = match re_path.captures(&caps_without_http[0]) {
            Some(val) => val,
            None => continue,
        };

        let host = re_delete_ports.replace_all(&caps_host[0], "").to_string();
        let mut path = caps_path[0].to_string().clone();

        loop {
            let old_path = path.clone();
            println!("{}{}", &host, &path);
            path = re_delete_path.replace_all(&path, "").to_string();
            if path == old_path {
                break;
            }
        }
    }
}
